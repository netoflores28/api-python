from app import app, mongo
from bson.json_util import dumps
from bson.objectid import ObjectId
from flask import jsonify, flash, request
from werkzeug import generate_password_hash, check_password_hash

@app.route('/dictionary')
def dictionary():
    dictionary = mongo.db.dictionary.find()
    resp = dumps(dictionary)
    return resp

@app.route('/dictionary/<mgr_id>')
def dictionaryByMgrId(mgr_id):
    dictionary = mongo.db.dictionary.find_one({'MGR_ID': mgr_id})
    resp = dumps(dictionary)
    return resp

@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not Found: ' + request.url,
    }
    resp = jsonify(message)
    resp.status_code = 404

    return resp


if __name__ == "__main__":
    print("WELCOME TO THE JUNGLE")
    app.run()